# Micromemo

A simple game made for microbit and designed for childs.

# Programma

## Crea array
Creiamo un array contenente 3 immagini scelte casualmente.  
Estriamo casualmente da questo array la posizione di 1 immagine e la salviamo in una variabile esterna.  

## Funzione: mostra_immagine
Si prende l'immagine della variabile esterna e la si mostra all'utente per 5 secondi.  
Pulisco il display.  

## Funzione: scorri_array
Con il pulsante sinistro si scorre l'array.  

## Funzione: scegli_immagine
Con il pulsante destro si sceglie l'immagine.  
Se si seleziona l'imagine mostrata in precedenza lo schermo diverrà **:)**.  
Altrimenti **:(**.  